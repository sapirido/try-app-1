import { AuthService } from './../auth.service';
import { ArticlesService } from './../articles.service';
import { Article } from './../interfaces/article';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-articles-list',
  templateUrl: './articles-list.component.html',
  styleUrls: ['./articles-list.component.css']
})
export class ArticlesListComponent implements OnInit {
 dataSource:Article[] = [];
 userId:string;
 dataSource$;
 displayedColumns: string[] = ['text','category'];
 
 constructor(private articleService:ArticlesService, private auth:AuthService) { }
 
  ngOnInit(): void {
    this.auth.getUser().subscribe(user =>{
      this.userId = user.uid;
      this.dataSource$ = this.articleService.getArticles(this.userId);
      this.dataSource$.subscribe(
        docs => {
          this.dataSource = [];
          console.log(docs.length);
          for(let document of docs){
            console.log({data:document.payload.doc.data()});
            const article:Article = document.payload.doc.data();
            this.dataSource.push(article);
          }
        }
      )
    }
    )
    console.log({dataSource:this.dataSource});
  }

}
