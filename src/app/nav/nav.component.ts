import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AuthService } from './../auth.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {

  $userEmail:Observable<string> = this.authService.getUserEmail();
  userEmail:string;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

    handleLogout(){
      this.authService.logout();
      this.router.navigate(['login']);
      
    }

  constructor(private breakpointObserver: BreakpointObserver,
    public authService : AuthService,private router:Router) {
    }

    ngOnInit(): void {
      this.$userEmail.subscribe(
        res =>
        this.userEmail = res
      )
    }
}
