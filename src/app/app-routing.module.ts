import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArticleComponent } from './article/article.component';
import { ArticlesListComponent } from './articles-list/articles-list.component';
import { ClassifyComponent } from './classify/classify.component';
import { LoginSuccessfulComponent } from './login-successful/login-successful.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  {path: 'login', component:LoginComponent},
  { path: 'register', component: RegisterComponent },
  {path: 'login/success', component: LoginSuccessfulComponent},
  {path:'article', component:ArticleComponent},
  {path:'classify/:article', component:ClassifyComponent},
  {path:'list',component:ArticlesListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
