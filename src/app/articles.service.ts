import { AuthService } from './auth.service';
import { Injectable, OnInit } from '@angular/core';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService  {
  articlesCollection:AngularFirestoreCollection;
  usersCollection:AngularFirestoreCollection = this.db.collection('users');

  addArticle(userId:string,text:string,category:string){
    const article = {text:text, category:category};
    this.usersCollection.doc(userId).collection('articles').add(article);
  }

  getArticles(userId:string){
    this.articlesCollection =  this.usersCollection.doc(userId).collection('articles');
    return this.articlesCollection.snapshotChanges();
  }

  constructor(private db:AngularFirestore) { }


}
