import { Component, OnInit } from '@angular/core';
import {CdkTextareaAutosize} from '@angular/cdk/text-field';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  article:string;
  
  onSubmit(){
    console.log('submited');
    this.router.navigate(['/classify',this.article]);

  }

  constructor(private router:Router) { }

  ngOnInit(): void {

  }

}
