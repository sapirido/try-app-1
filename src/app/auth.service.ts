import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { User } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user:Observable<User | null>;

  
  login(email:string, password:string){
    return this.afAuth.signInWithEmailAndPassword(email,password);
  }
  

  register(email:string,password:string){
    return this.afAuth.createUserWithEmailAndPassword(email,password);
  }
  
  logout(){
    this.afAuth.signOut();
  }

  getUser(): Observable<User | null> {
    return this.user;
  }

  public getUserEmail():Observable<string | null>{
    return this.getUser().pipe(map(user => user.email));
  }

  constructor(private afAuth:AngularFireAuth, private router:Router) {
    console.log(this.afAuth.authState);
    this.user = this.afAuth.authState;
   }
}
