import { AuthService } from './../auth.service';
import { Article } from './../interfaces/article';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClassifyService } from '../classify.service';
import { Category } from '../interfaces/category';
import { ArticlesService } from '../articles.service';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})

export class ClassifyComponent implements OnInit {
  
  getArticle:string;
  selectedValue: string;
  category:string = 'No category';
  userId:string;
  categories: Category[] = [
    {value: '0', viewValue: 'Business'},
    {value: '1', viewValue: 'Entertainment'},
    {value: '2', viewValue: 'Politics'},
    {value: '3', viewValue: 'Sport'},
    {value: '4', viewValue: 'Tech'}
  ];


  createArticle(selectedValue){
    console.log(selectedValue);
    const selectedCategory:Category = this.categories.find(cat => cat.value ===selectedValue);
    console.log({selectedCategory,viewVal:selectedCategory.viewValue})
    this.articlesService.addArticle(this.userId,this.getArticle,selectedCategory.viewValue);
  }
  // article:Article

  constructor(private route:ActivatedRoute,
              private classifyservice:ClassifyService,
              private articlesService:ArticlesService,
              private auth:AuthService) {}

  ngOnInit(): void {
      this.auth.getUser().subscribe(
        user => this.userId = user.uid
      )
      this.getArticle = this.route.snapshot.params.article;
      //classify
        this.classifyservice.classify(this.getArticle).subscribe(
          res => {
            console.log(res);
            this.category = this.classifyservice.categories[res];
            this.selectedValue=res;
          })
  }
}

