// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAg56HneD8lKmwK0-n4s734U4O4hkKUAgs",
    authDomain: "try-app-1.firebaseapp.com",
    projectId: "try-app-1",
    storageBucket: "try-app-1.appspot.com",
    messagingSenderId: "1012459032547",
    appId: "1:1012459032547:web:d1c1103ee76869d9e05ad6"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
